# [Mega Bundle of 3 graphic packs & 17 Bootstrap HTML, Vue & React Templates - keenthemes](https://keenthemes.com/products/templates-mega-bundle)

2023 Templates Bundle - a great value with 85% off to launch your next projects in the shortest amount of development
time at a ridiculously low cost using our 17 complete Pro templates, Laravel 10 & Asp.Net Core 7 Starter Kits, and 3
graphic packs in each template.

- Version v1.1.6
- Type Pro
- Category Pro Templates , Pro Graphics

# Content

- [KeenIcons 1.0 - 560+ high quality icons in 3 perfect styles](https://keenthemes.com/keenicons)
- [Auth Bundle - 8 Sets of Crafted 48 Vector Illustrations](https://keenthemes.com/auth-bundle)
- [Volume - 180 Vector Illustrations 6 Colors, Dark & Light Options](https://keenthemes.com/volume)
- [Axel HTML Pro - Bootstrap 5 Admin Dashboard Theme](https://keenthemes.com/products/axel-html-pro)
- [Blaze HTML Pro - Bootstrap 5 Admin Dashboard Theme](https://keenthemes.com/products/blaze-html-pro)
- [Oswald HTML Pro - Bootstrap 5 Admin Dashboard Theme](https://keenthemes.com/products/oswald-html-pro)
- [Saul HTML Pro - Bootstrap 5 Admin Dashboard Theme](https://keenthemes.com/products/saul-html-pro)
- [Open HTML Pro - Bootstrap 5 & Laravel 10 Template](https://keenthemes.com/products/open-html-pro)
- [Star HTML Pro - Bootstrap 5 & Asp.Net Core 7 Template](https://keenthemes.com/products/star-html-pro)
- [Rider HTML Pro - Bootstrap 5 Admin Dashboard Template](https://keenthemes.com/products/rider-html-pro)
- [Rider Vue Pro - Admin Dashboard Template](https://keenthemes.com/products/rider-vue-pro)
- [Ceres HTML Pro - Admin Dashboard Template](https://keenthemes.com/products/ceres-html-pro)
- [Start HTML Pro - Admin Dashboard Template](https://keenthemes.com/products/start-html-pro)
- [Start Vue Pro - Admin Dashboard Template](https://keenthemes.com/products/start-vue-pro)
- [Start React Pro - Admin Dashboard Template](https://keenthemes.com/products/start-react-pro)
- [Seven HTML Pro - Admin Dashboard Template](https://keenthemes.com/products/seven-html-pro)
- [Jet HTML Pro - Multipurpose Dashboard Template](https://keenthemes.com/products/jet-html-pro)